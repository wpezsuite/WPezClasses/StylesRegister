<?php

namespace WPezSuite\WPezClasses\StylesRegister;

// No WP? Die! Now!!
if ( ! defined( 'ABSPATH' ) ) {
	header( 'HTTP/1.0 403 Forbidden' );
	die();
}

class ClassHooks {

	protected $_new_component;
	protected $_arr_hook_defaults;
	protected $_arr_actions;
	protected $_arr_filters;

	public function __construct( InterfaceStylesRegister $obj ) {

		$this->_new_component = $obj;

		$this->setPropertyDefaults();

	}

	protected function setPropertyDefaults() {

		$this->_arr_hook_defaults = [
			'active'        => true,
			'component'     => $this->_new_component,
			'priority'      => 10,
			'accepted_args' => 1
		];

		// actions
		$this->_arr_actions = [];

		// --- admin
		$this->_arr_actions['admin_enqueue_scripts_register'] = [
			'hook'     => 'admin_enqueue_scripts',
			'callback' => 'registerAdmin',
		];

		$this->_arr_actions['admin_enqueue_scripts_enqueue'] = [
			'hook'     => 'admin_enqueue_scripts',
			'callback' => 'enqueueAdmin',
		];

		// --- front
		$this->_arr_actions['wp_enqueue_scripts_register'] = [
			'hook'     => 'wp_enqueue_scripts',
			'callback' => 'registerFront',
		];

		$this->_arr_actions['wp_enqueue_scripts_enqueue'] = [
			'hook'     => 'wp_enqueue_scripts',
			'callback' => 'enqueueFront',
		];

		// --- login
		$this->_arr_actions['login_enqueue_scripts_register'] = [
			'hook'     => 'login_enqueue_scripts',
			'callback' => 'registerLogin',
		];

		$this->_arr_actions['login_enqueue_scripts_enqueue'] = [
			'hook'     => 'login_enqueue_scripts',
			'callback' => 'enqueueLogin',
		];

		// --- block (both)
		$this->_arr_actions['enqueue_block_assets_register'] = [
			'hook'     => 'enqueue_block_assets',
			'callback' => 'registerBlock',
		];

		$this->_arr_actions['enqueue_block_assets_enqueue'] = [
			'hook'     => 'enqueue_block_assets',
			'callback' => 'enqueueBlock',
		];

		// --- block_admin
		$this->_arr_actions['enqueue_block_editor_assets_register'] = [
			'hook'     => 'enqueue_block_editor_assets',
			'callback' => 'registerBlockAdmin',
		];

		$this->_arr_actions['enqueue_block_editor_assets_enqueue'] = [
			'hook'     => 'enqueue_block_editor_assets',
			'callback' => 'enqueueBlockAdmin',
		];

		// --- block_front
		$this->_arr_actions['enqueue_block_assets_front_register'] = [
			'hook'     => 'enqueue_block_assets',
			'callback' => 'registerBlockFront',
		];

		$this->_arr_actions['enqueue_block_assets_front_enqueue'] = [
			'hook'     => 'enqueue_block_assets',
			'callback' => 'enqueueBlockFront',
		];

		// filters
		$this->_arr_filters = [];

		/*
		$this->_arr_filters['hook'] = [
			'hook'     => 'hook',
			'callback' => 'TODO',
		];
		*/

	}


	/**
	 * @param bool $arr
	 *
	 * @return bool
	 */
	public function updateHookDefaults( $arr = false ) {

		if ( is_array( $arr ) ) {

			$this->_arr_hook_defaults = array_merge( $this->_arr_hook_defaults, $arr );

			return true;

		}

		return false;
	}

	/**
	 * @return mixed
	 */
	public function getActions() {

		return $this->_arr_actions;
	}


	/**
	 * @param bool $arr
	 *
	 * @return bool
	 */
	public function updateActions( $arr = false ) {

		if ( is_array( $arr ) ) {

			$this->_arr_actions = array_merge( $this->_arr_actions, $arr );

			return true;

		}

		return false;
	}

	/**
	 * @return mixed
	 */
	public function getFilters() {

		return $this->_arr_filters;
	}


	/**
	 * @param bool $arr
	 *
	 * @return bool
	 */
	public function updateFilters( $arr = false ) {

		if ( is_array( $arr ) ) {

			$this->_arr_filters = array_merge( $this->_arr_filters, $arr );

			return true;

		}

		return false;
	}


	protected function registerMaster( $arr_exclude, $arr_hooks, $str_wpfn = 'add_action' ) {


		foreach ( $arr_hooks as $str_ndx => $arr_hook ) {

			if ( in_array( $str_ndx, $arr_exclude ) ) {
				continue;
			}

			$arr = array_merge( $this->_arr_hook_defaults, $arr_hook );
			if ( $arr['active'] === false ) {
				continue;
			}

			$str_wpfn(
				$arr['hook'],
				[ $arr['component'], $arr['callback'] ],
				$arr['priority'],
				$arr['accepted_args']
			);
		}
	}


	/**
	 * @param bool $arr_exclude
	 */
	public function register( $arr_exclude = false ) {

		if ( ! is_array( $arr_exclude ) ) {
			$arr_exclude = [];
		}

		if ( ! empty( $this->_arr_actions ) ) {

			$this->registerMaster( $arr_exclude, $this->_arr_actions, 'add_action' );

		}

		if ( ! empty( $this->_arr_filters ) ) {

			$this->registerMaster( $arr_exclude, $this->_arr_filters, 'add_filter' );

		}

	}

}