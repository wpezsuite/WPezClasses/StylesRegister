<?php

namespace WPezSuite\WPezClasses\StylesRegister;

interface InterfaceStylesRegister {

	public function registerAdmin();

	public function registerFront();

	public function registerLogin();

	public function registerBlock();

	public function registerBlockAdmin();

	public function registerBlockFront();

	public function enqueueAdmin();

	public function enqueueFront();

	public function enqueueLogin();

	public function enqueueBlock();

	public function enqueueBlockAdmin();

	public function enqueueBlockFront();


}