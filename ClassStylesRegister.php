<?php
/**
 * Created by PhpStorm.
 */


namespace WPezSuite\WPezClasses\StylesRegister;

class ClassStylesRegister implements InterfaceStylesRegister {

	protected $_arr_styles;
	protected $_arr_style_defaults;
	protected $_arr_hooks;
	protected $_arr_registered;
	protected $_arr_key_values;
	protected $_arr_media_supported;


	public function __construct() {

		$this->setPropertyDefaults();

	}


	public function setPropertyDefaults() {

		$this->_arr_styles = [];

		$this->_arr_style_defaults = [
			'active'         => true,
			'handle'         => false, // required
			'src'            => false, // required
			'deps'           => [],
			'ver'            => false,
			'media'          => 'all',
			// where are we using this style sheet/
			'hooks'          => [],
			// https://developer.wordpress.org/reference/functions/wp_style_add_data/
			'add_data_key'   => false,
			'add_data_value' => false
		];

		$this->_arr_hooks = [
			'admin',
			'front',
			'login',
			'block',
			'block_admin',
			'block_front'
		];

		$this->_arr_registered = [];

		$this->_arr_key_values = [
			'conditional',
			'rtl',
			'suffix',
			'alt',
			'title'
		];

		$this->_arr_media_supported = [
			'all',
			'braille',
			'embossed',
			'handheld',
			'print',
			'projection',
			'screen',
			'speech',
			'tty',
			'tv'
		];
	}


	public function updateStyleDefaults( $arr_defaults = false ) {

		if ( is_array( $arr_defaults ) ) {

			$this->_arr_style_defaults = array_merge( $this->_arr_style_defaults, $arr_defaults );

			return true;
		}

		return false;
	}


	public function pushStyle( $arr_style = false ) {

		if ( is_array( $arr_style ) ) {

			$arr_temp = array_merge( $this->_arr_style_defaults, $arr_style );
			if ( is_string( $arr_temp['handle'] ) && is_string( $arr_temp['src'] ) && is_array( $arr_temp['hooks'] ) ) {

				$this->_arr_styles[] = $arr_temp;

				return true;
			}
		}

		return false;
	}


	public function loadStyles( $arr_styles = false ) {

		if ( is_array( $arr_styles ) ) {
			$arr_ret = [];
			foreach ( $arr_styles as $key => $arr_style ) {

				$arr_ret[ $key ] = $this->pushStyle( $arr_style );
			}

			return $arr_ret;
		}

		return false;
	}

	public function getStyles() {

		return $this->_arr_styles;
	}


	public function registerAdmin() {

		$this->registerMaster( 'admin' );
	}

	public function registerFront() {

		$this->registerMaster( 'front' );
	}

	public function registerLogin() {

		$this->registerMaster( 'login' );
	}

	public function registerBlock() {

		$this->registerMaster( 'block' );
	}

	public function registerBlockAdmin() {

		$this->registerMaster( 'block_admin' );
	}

	public function registerBlockFront() {

		if ( is_admin() ) {
			return;
		}

		$this->registerMaster( 'block_front' );
	}


	protected function registerMaster( $str_hook = 'front' ) {

		foreach ( $this->_arr_styles as $arr_style ) {

			if ( $arr_style['active'] === false || in_array( $str_hook, $arr_style['hooks'] ) === false ) {
				continue;
			}

			$obj = (object) $arr_style;

			if ( in_array( $obj->media, $this->_arr_media_supported ) === false ) {
				$obj->media = 'all';
			}

			$bool_return = wp_register_style(
				$obj->handle,
				$obj->src,
				$obj->deps,
				$obj->ver,
				$obj->media
			);

			// Note - Scripts will be different as WP as pre-registered scripts
			if ( $bool_return === true ) {

				$this->_arr_registered[ $str_hook ][ $obj->handle ] = $arr_style;

			}
		}
	}

	public function enqueueAdmin() {

		$this->enqueueMaster( 'admin' );
	}

	public function enqueueFront() {

		$this->enqueueMaster( 'front' );
	}

	public function enqueueLogin() {

		$this->enqueueMaster( 'login' );
	}

	public function enqueueBlock() {

		$this->enqueueMaster( 'block' );
	}

	public function enqueueBlockAdmin() {

		$this->enqueueMaster( 'block_admin' );
	}

	public function enqueueBlockFront() {

		if ( is_admin() ) {
			return;
		}

		$this->enqueueMaster( 'block_front' );
	}

	protected function enqueueMaster( $str_hook = 'front' ) {

		if ( ! isset( $this->_arr_registered[ $str_hook ] ) || ! is_array( $this->_arr_registered[ $str_hook ] ) ) {
			return;
		}

		foreach ( $this->_arr_registered[ $str_hook ] as $str_handle => $arr_style ) {

			$obj = (object) $arr_style;
			wp_enqueue_style( $obj->handle );

			if ( is_string( $obj->add_data_key ) && ! empty( $obj->add_data_key ) && in_array( strtolower( $obj->add_data_key ), $this->_arr_key_values ) && is_string( $obj->add_data_value ) ) {

				// TODO - this returns a bool. let's keep passing it back
				wp_style_add_data( $obj->handle, $obj->add_data_key, $obj->add_data_value );
			}
		}
	}

}